#!/bin/bash
# vim: et sts=4 sw=4

# <https://wiki.debian.org/SuitesAndReposExtension>
#
# Notes:
#
# ~ Keyring handling (still WIP)
#
# For Debian and Ubuntu: chdist handles it by itself, providing that the
# keyrings are already installed in /usr/share/keyrings/. So we have nothing to
# do? Note that on Debian, one must install the package ubuntu-archive-keyring,
# we can't always assume it's the case, so Ubuntu doesn't necessarily works out
# of the box. Also, note that on Ubuntu, the package is named ubuntu-keyring.
#
# Ok. Next, what about other distros? Assuming we know the name of the keyring,
# we can check if it's present in usr/share/keyrings, people might have
# downloaded it there, or installed a keyring package from another distro.
#
# Ok, but next, what if the keyring can't be found locally? Can we be smart
# enough to fetch it automatically?
#
# One approach: try to download the keyring package, and extract the keyring
# from there. We can easily fetch the Packages file, then find the keyring
# package, download it... then, we can't *install* it in the chdist, since a
# chdist is not a chroot. All we can do is unpack it, but that's not always
# enough, some distros (eg. kali) install the keyring in usr/share/keyrings,
# and then symlink it in etc/apt/trusted.gpg.d via a postinst script. It means
# that, after unpacking the keyring in the chdist, we must manually symlink the
# keyring, depending on the distro... Not as easy as I thought, but it seems to
# work.
# 
# Another approach: fetch the keyring from out there. Often enough, the keyring
# can be fetched from a well-known location over HTTPS. But not always, for
# example, ubuntu/project/ubuntu-archive-keyring.gpg seems to be way outdated,
# and I couldn't find any other up-to-date key out there.
#
# Another approach: let user specify one (or more) keyring paths. Ie. we let
# user find the keyrings needed by themselves, and then they pass it in
# argument. It's always useful, for cases where we can't do better.
#
# List of keyring urls (for later):
# * ms: https://packages.microsoft.com/keys/microsoft.asc 
#
# ~ To be improved:
#
# * Better command-line parsing: enter numbers for options.
# * How to support foreign architectures? For now we just hardcode 'armhf' for
#   Raspberry. We need a way to list arch available per distro. And allow user
#   to select it. Then finally bake that in the target dir, I'd say as a suffix,
#   and no suffix if the arch is the dpkg native arch.
# * Add options for most command things (rather than env vars)

set -eu

DISTRO=
RELEASE=
COMPONENTS=

ARCH=
APT_PROXY=   # XXX should have a flag, default to auto
APT_UPDATE=1
DATA_DIR=~/.chdist
DEB_SRC=1
FORCE=0
TARGET=

ALT_KEYRING_PATH=${ALT_KEYRING_PATH:-}   # A directory to look for keyrings,
                                         # to be set by caller.

USAGE="Usage: $(basename $0) [OPTION]... [DISTRO] [RELEASE] [COMPONENTS]

  -a, --arch ARCHITECTURE
                   Choose architecture (default: dpkg --print-architecture)
  -d, --data-dir DATA_DIR
                   Directory to create the chdist into (default: ~/.chdist/)
  -f, --force      Overwrite destination if it already exists
  -h, --help       Show this help message
  -t, --target TARGET
                   Name of the chdist, ie. directory created under DATA_DIR.
                   If not specified,  it will be DISTRO-RELEASE,  or simply
                   RELEASE if the release name already starts with DISTRO-,
                   or if the distro is Debian. The suffix -ARCH is appended
                   in case of foreign architecture.
  --no-apt-update  Don't run 'apt update' and 'apt-file update' at the end
  --no-deb-src     Don't add deb-src to the sources.list
"

fail() { echo "$@" >&2; exit 1; }

in_list() {
    local word=$1 && shift
    local item=
    for item in "$@"; do
        [ "$item" = "$word" ] && return 0
    done
    return 1
}

while true; do
    [ $# -eq 0 ] && break
    case $1 in
        (-a|--architecture) ARCH=$2; shift ;;
        (-d|--data-dir) DATA_DIR=$2; shift ;;
        (-f|--force) FORCE=1 ;;
        (-h|--help) echo "$USAGE"; exit 0 ;;
        (-t|--target) TARGET=$2; shift ;;
        (--no-apt-update) APT_UPDATE=0 ;;
        (--no-deb-src) DEB_SRC=0 ;;
        (*)
            if [ -z "$DISTRO" ]; then
                DISTRO=$1
            elif [ -z "$RELEASE" ]; then
                RELEASE=$1
            elif [ -z "$COMPONENTS" ]; then
                COMPONENTS=$1
            else
                fail "Unexpected argument"
            fi
            ;;
    esac
    shift
done

# have python3, python3-jinja2 and python3-yaml installed
command -v grep-dctrl >/dev/null || fail "Please install dctrl-tools"
command -v file >/dev/null || fail "Please install file"
command -v j2 >/dev/null || fail "Please install j2cli"
command -v wget >/dev/null || fail "Please install wget"


# Detect apt-cacher-ng
if (</dev/tcp/localhost/3142) 2>/dev/null; then
    echo "Looks like apt-cacher-ng is listening on port 3142."
    APT_PROXY=http://localhost:3142
fi


# Distros

ALL_DISTROS=$(./parse.py dists)

if [ -z "$DISTRO" ]; then
    echo "Distributions: $ALL_DISTROS"
    read -p "Pick one: " DISTRO
fi

in_list $DISTRO $ALL_DISTROS || \
    fail "Invalid distribution: $DISTRO"


# Releases

get_releases() {
    ./parse.py releases $1
}

get_aliases() {
    ./parse.py aliases $1
}

ALL_RELEASES=$(get_releases $DISTRO)
ALL_ALIASES=$(get_aliases $DISTRO)

[ "$ALL_RELEASES" ] || \
    fail "Invalid distribution: $DISTRO"

if [ -z "$RELEASE" ]; then
    echo "Releases:" $ALL_RELEASES
    if [ "$ALL_ALIASES" ]; then
        echo "Aliases :" $ALL_ALIASES
    fi
    read -p "Pick one: " RELEASE
fi

in_list $RELEASE $ALL_RELEASES $ALL_ALIASES || \
    fail "Invalid release: $RELEASE"


# Mirror

get_mirror() {
    ./parse.py mirror $1 $2
}

get_security_mirror() {
    ./parse.py security_mirror $1 $2
}

MIRROR=$(get_mirror $DISTRO $RELEASE)
SECURITY_MIRROR=$(get_security_mirror $DISTRO $RELEASE)

[ "$MIRROR" ] || \
    fail "Invalid distribution: $DISTRO"


# Resolve some aliases (after we have a mirror)

if in_list $RELEASE $ALL_ALIASES; then
    ALIAS=$RELEASE
    echo -n "Resolving alias '$ALIAS'... "
    RELEASE=$(wget -q -O- $MIRROR/dists/$ALIAS/Release \
        | sed -n "s/^Codename: //p")
    [ "$RELEASE" ] || fail "Failed!"
    echo "$RELEASE"
fi


# Components

get_components() {
    ./parse.py components $1 $2
}

ALL_COMPONENTS=$(get_components $DISTRO $RELEASE)

[ "$ALL_COMPONENTS" ] || \
    fail "Invalid distribution: $DISTRO"

if [ -z "$COMPONENTS" ]; then
    echo "Components:" $ALL_COMPONENTS
    read -p "Pick some, leave empty for all: " COMPONENTS
fi

if [ -z "$COMPONENTS" ]; then
    COMPONENTS=$ALL_COMPONENTS
fi

for comp in $COMPONENTS; do
    in_list $comp $ALL_COMPONENTS || \
       fail "Invalid component: $comp"
done


# Templating

get_template() {
    ./parse.py template $1 $2
}

TEMPLATE=$(get_template $DISTRO $RELEASE)

[ "$TEMPLATE" ] || \
    fail "Invalid distribution: $DISTRO"


# Do the job

echo "----------------------------"
echo "Distro    : $DISTRO"
echo "Release   : $RELEASE"
echo "Components: $COMPONENTS"
echo "Mirror    : $MIRROR"
if [ "$SECURITY_MIRROR" ]; then
    echo "Sec Mirror: $SECURITY_MIRROR"
fi
echo "----------------------------"

read -p "Press enter to proceed "

# XXX Ideally we'd know what architectures are supported by the distro,
# for example raspbian only supports armhf. What makes it tricky is that
# raspbian is not really a distro, it's one of the suites included in
# the raspberrypi distro, prior to the bookworm release.
HOST_ARCH=$(dpkg --print-architecture)
if [ -z "$ARCH" ]; then
    ARCH=$HOST_ARCH
fi

if ! [ -d $DATA_DIR ]; then
    mkdir $DATA_DIR
fi

if [ -z "$TARGET" ]; then
    if [ "$DISTRO" = debian ]; then
        TARGET=$RELEASE
    elif echo "$RELEASE" | grep -q "^$DISTRO-"; then
        TARGET=$RELEASE
    else
        TARGET=$DISTRO-$RELEASE
    fi
    if [ "$ARCH" != "$HOST_ARCH" ]; then
        TARGET=$TARGET-$ARCH
    fi
fi

CHDIST_DIR=$DATA_DIR/$TARGET

if [ -d "$CHDIST_DIR" ]; then
    if (( FORCE )); then
        echo "Removing existing directory: $CHDIST_DIR"
    else
        echo "Directory $CHDIST_DIR already exists!"
        read -p "Press enter to remove it, or <Ctrl+C> to abort "
    fi
    rm -fr "$CHDIST_DIR"
fi


# Create the chdist

create_chdist() {

    # Re-write of 'chdist create'. It does pretty much the same thing,
    # except that it doesn't create the sources.list, and it doesn't try
    # to handle keyrings either. This is because we do it after.

    local rootdir=$1
    local arch=$2
    local dir=

    mkdir $rootdir
    cd $rootdir

    mkdir -p etc/apt
    for dir in apt.conf preferences trusted.gpg sources.list; do
        mkdir etc/apt/${dir}.d
    done

    mkdir -p var/cache/apt/archives/partial
    mkdir -p var/lib/apt/lists/partial
    mkdir -p var/lib/dpkg

    touch var/lib/dpkg/status

    cat << EOF > etc/apt/apt.conf
Apt {
   Architecture "$arch";
   Architectures "$arch";
};

Dir "$rootdir";
EOF

    cd $OLDPWD
}

# Use our own implementation, save us from depending on devscripts,
# and in any case we don't use the main features of 'chdist create',
# namely setting up the sources.list, and setting up keyring.
#chdist --arch $ARCH create $TARGET >/dev/null
create_chdist $CHDIST_DIR $ARCH
echo "Created chdist tree: $CHDIST_DIR"


# Setup the keyrings

get_keyrings() {
    ./parse.py keyrings $1 $2
}

KEYRINGS=$(get_keyrings $DISTRO $RELEASE)

[ "$KEYRINGS" ] || \
    fail "No keyring package for $DISTRO $RELEASE"

symlink_keyring() {

    # Symlink keyring passed in argument into the chdist tree.
    #
    # There is limited support for passing argument to the ln command,
    # it must come befores positional arguments.

    local rootdir=
    local keyring=
    local ln=ln

    while [ $# -gt 0 ]; do
        case $1 in
            -*)
                ln="$ln $1"
                ;;
            *)
                if [ -z "$rootdir" ]; then
                    rootdir=$1
                elif [ -z "$keyring" ]; then
                    keyring=$1
                fi
                ;;
        esac
        shift
    done

    # Avoid a symlink pointing to another symlink, it's ugly
    keyring=$(realpath $keyring)

    echo "Symlinking keyring : $keyring"
    cd $rootdir/etc/apt/trusted.gpg.d/
    $ln -fsv $keyring
    cd $OLDPWD
}

fetch_and_extract_keyring() {

    # Knowing the name of a keyring package, download the .deb and unpack it
    # into the chdist tree.
    #
    # The whole thing is a bit of a hack. We can't *install* the package, as we
    # can't chroot inside the chdist directory, afaik. So we can only *extract*
    # the package, but for a keyring it's not enough, as often the keyring is
    # installed into usr/share/keyrings, and then symlinked into
    # etc/apt/trusted.gpg.d via a postinst script. Since we only extract the
    # package, the postinst script doesn't run, so we have to symlink the
    # keyring manually...

    local rootdir=$1
    local keyring=$2
    local mirror=$3
    local component=main
    local path=

    pushd $rootdir >/dev/null

    [ -d var/local ] || mkdir var/local
    pushd var/local >/dev/null

    # XXX assume gz compression
    path=dists/$RELEASE/$component/binary-$ARCH/Packages.gz
    echo "Downloading index  : $mirror/$path ..."
    wget -q $mirror/$path
    gunzip Packages.gz

    path=$(grep-dctrl -PX $keyring -ns Filename Packages || :)
    [ "$path" ] || fail "Package $keyring not found in index!"
    echo "Downloading package: $mirror/$path ..."
    wget -q $mirror/$path

    rm -f Packages
    popd >/dev/null

    dpkg --extract var/local/$(basename $path) .

    popd >/dev/null
}

setup_keyrings_from_package() {
    local rootdir=$1 && shift
    local package=$1 && shift
    local mirror=$1 && shift
    local keyring=
    local missing=

    # Symlink local keyrings if they're found
    missing=0
    for keyring in "$@"; do
        keyring=/usr/share/keyrings/$keyring
        if [ -e $keyring ]; then
            symlink_keyring $rootdir $keyring
        else
            (( missing+=1 ))
        fi
    done

    # Return if all keyrings were symlinked
    if ! (( missing )); then
        return 0
    fi

    # Some keyring(s) missing, fetch the keyring package
    fetch_and_extract_keyring $rootdir $package $mirror

    # Symlink keyrings that have been extracted in the chdist
    # XXX assumes keyrings were extracted under usr/share/keyrings, but that's
    # not always the case, sometimes they are also in etc/apt/trusted.gpg.d/.
    missing=0
    for keyring in "$@"; do
        keyring=$CHDIST_DIR/usr/share/keyrings/$keyring
        if [ -e $keyring ]; then
            symlink_keyring -r $rootdir $keyring
        else
            (( missing+=1 ))
        fi
    done

    # Error out if keyrings are still missing
    if (( missing )); then
        fail "$keyrings_missing keyring(s) still missing at this point!"
    fi
}

setup_keyring_from_url() {
    local rootdir=$1
    local url=$2
    local fn=
    local mime=

    cd $rootdir/etc/apt/trusted.gpg.d

    echo "Downloading keyring: $url ..."
    wget -q $url

    # Make sure files have the right extension
    fn=$(basename $url)
    mime=$(file -bi $fn)
    case $mime in
        ("application/pgp-keys; charset=binary")
            if [ "${fn##*.}" != "gpg" ]; then
                mv -v $fn $fn.gpg
            fi
            ;;
        ("application/pgp-keys; charset=us-ascii")
            if [ "${fn##*.}" != "asc" ]; then
                mv -v $fn $fn.asc
            fi
            ;;
        (*)
            echo "Unexpected mime type for GPG keyring: $mime"
            echo "All bets are off!"
            ;;
    esac

    cd $OLDPWD
}

while read -r keyring_line; do
    if grep -q "^http" <<< $keyring_line; then
        setup_keyring_from_url $CHDIST_DIR $keyring_line
    else
        setup_keyrings_from_package $CHDIST_DIR $keyring_line
    fi
done <<< $KEYRINGS


# Setup the sources.list

(
    export MIRROR SECURITY_MIRROR RELEASE COMPONENTS
    j2 templates/$TEMPLATE.j2 > $CHDIST_DIR/etc/apt/sources.list
)

if (( DEB_SRC )); then
    echo "Enabling deb-src in APT sources.list"
    {
        echo "# Source packages"
        sed -n 's/^deb /deb-src /p' $CHDIST_DIR/etc/apt/sources.list
    } >> $CHDIST_DIR/etc/apt/sources.list
fi


# Configure APT proxy

if [ "$APT_PROXY" ]; then
    echo "Configuring proxy: ${APT_PROXY}"
    {
        echo "Acquire::HTTP::Proxy \"${APT_PROXY}\";"
        echo "Acquire::HTTPS::Proxy \"false\";"
    } > $CHDIST_DIR/etc/apt/apt.conf.d/02proxy
fi


# Initialize APT cache

# devscripts might not be installed (eg. in CI)
HAVE_CHDIST=0
command -v chdist >/dev/null && HAVE_CHDIST=1

if (( APT_UPDATE )); then
    echo "Running apt update ..."
    if (( HAVE_CHDIST )); then
        chdist -d $DATA_DIR apt $TARGET --error-on=any update
    else
        APT_CONFIG=$CHDIST_DIR/etc/apt/apt.conf apt --error-on=any update
    fi
    if command -v apt-file >/dev/null; then
        echo "Running apt-file update ..."
        HAVE_CHDIST=0
        if (( HAVE_CHDIST )); then
            chdist -d $DATA_DIR apt-file $TARGET update
        else
            conffiles=$(dpkg-query -W -f '${Conffiles}' apt-file \
                | awk '{print $1}' | grep '/apt\.conf\.d/' || :)
            for cf in $conffiles; do
                [ -e $cf ] || continue
                [ -e $CHDIST_DIR$cf ] && continue
                cp -v $cf ${CHDIST_DIR}${cf}
            done
            APT_CONFIG=$CHDIST_DIR/etc/apt/apt.conf apt-file update
        fi
    else
        echo "Not running apt-file update (apt-file is not installed)"
    fi
else
    echo
    echo "Now run the following commands to populate APT cache:"
    echo "chdist apt $TARGET update"
    echo "chdist apt-file $TARGET update"
    echo
fi

exit 0



#### below is work in progress ####

# Setup keyrings - via URL

KEYRINGS_URLS=

case $DISTRO in
    (kali)
        KEYRINGS_URLS=https://archive.kali.org/archive-key.asc
        ;;
    (parrot)
        KEYRINGS_URLS=https://deb.parrot.sh/parrot/keyring.gpg
        ;;
    (raspberrypi)
        case $RELEASE in
            (stretch|buster|bullseye)
                KEYRINGS_URLS=https://archive.raspberrypi.org/debian/raspberrypi.gpg.key
                # 2024-02-07: HTTPS didn't work
                KEYRINGS_URLS+=" http://raspbian.raspberrypi.org/raspbian.public.key"
                ;;
            (*)
                KEYRINGS_URLS=https://archive.raspberrypi.com/debian/raspberrypi.gpg.key
                ;;
        esac
        ;;
    (ubuntu)
        # NB: ubuntu-archive-keyring is _not_ in Debian stable, so we can't
        # expect it to be installed, we must support fetching it from outside
        # XXX Doesn't work, not the right key, or outdated
        KEYRINGS_URLS=http://archive.ubuntu.com/ubuntu/project/ubuntu-archive-keyring.gpg
        ;;
esac

setup_keyrings() {
    local rootdir=$1
    local distro=$2
    local distros=
    local dist=
    local keyring=

    cd $rootdir/etc/apt/trusted.gpg.d

    # Symlinks keyrings that are present locally. We look for Debian
    # keyrings and $distro keyring.
    if [ $distro != debian ]; then
        distros="debian $distro"
    else
        distros=$distro
    fi
    for dist in $distros; do
        for keyring in keyring removed-keys; do
            if [ -e /usr/share/keyrings/$dist-archive-$keyring.gpg ]; then
                ln -s /usr/share/keyrings/$dist-archive-$keyring.gpg
            fi
        done
    done

    # If there's no keyring for $distro, fetch it. The keyring must have the
    # right extension, either .gpg or .asc, and of course it's not always the
    # case.
    if ! [ -e $distro-archive-keyring.gpg ]; then
        local url=
        local fn=
        local mime=

        for url in $KEYRINGS_URLS; do
            echo "Downloading keyring: $url ..."
            wget $url

            # Make sure files have the right extension
            fn=$(basename $url)
            mime=$(file -bi $fn)
            case $mime in
                ("application/pgp-keys; charset=binary")
                    if [ "${fn##*.}" != "gpg" ]; then
                        mv -v $fn $fn.gpg
                    fi
                    ;;
                ("application/pgp-keys; charset=us-ascii")
                    if [ "${fn##*.}" != "asc" ]; then
                        mv -v $fn $fn.asc
                    fi
                    ;;
                (*)
                    echo "Unexpected mime type for GPG keyring: $mime"
                    echo "All bets are off!"
                    ;;
            esac
        done
    fi

    cd $OLDPWD
}

setup_keyrings $CHDIST_DIR $DISTRO



# Another approach for keyrings... 

KEYRING_FILES=

case $DISTRO in
    (kali)
        KEYRING_FILES="kali-archive-keyring.gpg"
        ;;
    (parrot)
        KEYRING_FILES="parrot-archive-keyring.gpg"
        ;;
    (raspberrypi)
        KEYRING_FILES="raspbian-archive-keyring.gpg"
        KEYRING_FILES+=" raspberrypi-archive-keyring.gpg"
        ;;
esac

if [ "$KEYRING_FILES" ]; then
    for keyring_f in $KEYRING_FILES; do
        keyring=/usr/share/keyrings/$keyring_f
        if [ -f "$keyring" ]; then
            ln -s $keyring $CHDIST_DIR/etc/apt/trusted.gpg.d/
            continue
        fi

        if [ "$ALT_KEYRING_PATH" ]; then
            keyring=$ALT_KEYRING_PATH/$keyring_f
            if [ -f "$keyring" ]; then
                install -m 0644 $keyring $CHDIST_DIR/etc/apt/trusted.gpg.d/
                continue
            fi
        fi

        echo >&2 "W: Keyring '$keyring_f' not found in '/usr/share/keyrings/'"
        echo >&2 "W: If keyrings are somewhere else, set ALT_KEYRING_PATH and retry."
        echo >&2 "W: Otherwise install it manually at $CHDIST_DIR/etc/apt/trusted.gpg.d/"
    done
fi

# How-to: Convert keyring with gpg. I suppose it can be useful
# if we target old versions of apt that don't support keyrings
# in the .asc format?
#
#    command -v gpg >/dev/null || fail "Please install gpg"
#
#    wget https://archive.raspbian.org/raspbian.public.key
#    GNUPGHOME=$(mktemp -d)
#    chmod 0700 $GNUPGHOME
#    export GNUPGHOME
#    gpg --no-keyring --no-default-keyring --no-auto-check-trustdb \
#        --no-options --import-options import-export --import \
#        < raspbian.public.key > raspbian-archive-keyring.gpg
#    mv raspbian-archive-keyring.gpg $CHDIST_DIR/etc/apt/trusted.gpg.d/
#    rm -fr $GNUPGHOME
#  rm raspbian.public.key
