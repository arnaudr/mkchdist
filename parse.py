#!/usr/bin/python3

import argparse
import sys
import urllib.request

import jinja2
import yaml

YAML_DATA = None
YAML_FILE = "mkchdist.yaml"

def resolve_alias(mirror, alias):

    print(f"Resolving alias {alias}... ", end="", file=sys.stderr)
    sys.stderr.flush()

    # fetch release file
    url = f"{mirror}/dists/{alias}/Release"
    with urllib.request.urlopen(url) as response:
       release_file = response.readlines()

    # find the codename
    codename = ""
    for line in release_file:
        line = line.decode("utf-8")
        if line.startswith("Codename: "):
            codename = line.split()[1]
            break

    print(codename, file=sys.stderr)

    return codename

def get_until_modifier(dist, release, required_field):
    # get the index of the release passed in argument
    releases = dist.get("releases", [])
    try:
        release_idx = releases.index(release)
    except ValueError:
        return None

    # get the right modifier
    modifier = None
    modifier_idx = -1
    modifiers = dist.get("modifiers", [])
    for m in modifiers:
        # discard modifiers that don't have the required field
        if not required_field in m:
            continue

        # discard modifier that don't have 'until_release' field
        mod_release = m.get("until_release", "")
        if not mod_release:
            continue

        # get index of the release for this modifier
        try:
            mod_release_idx = releases.index(mod_release)
        except ValueError:
            continue

        # keep the modifier if it applies
        if mod_release_idx >= release_idx:
            if not modifier:
                modifier = m
                modifier_idx = mod_release_idx
            elif mod_release_idx <= modifier_idx:
                modifier = m
                modifier_idx = mod_release_idx

    return modifier

def get_dists(args):
    dists = [x for x in YAML_DATA if not x.startswith(".")]
    if dists:
        print(" ".join(dists))

def get_releases(args):
    dist = YAML_DATA.get(args.dist, {})
    releases = dist.get("releases", [])
    if releases:
        print(" ".join(releases))

def get_aliases(args):
    dist = YAML_DATA.get(args.dist, {})
    aliases = dist.get("aliases", [])
    if aliases:
        print(" ".join(aliases))

def get_mirror(args):
    dist = YAML_DATA.get(args.dist, {})
    aliases = dist.get("aliases", [])

    # if it's an alias, resolve it
    if args.release in aliases:
        release = resolve_alias(dist["mirror"], args.release)
        if not release:
            return
        args.release = release

    # get the modifier that applies, if any
    modifier = get_until_modifier(dist, args.release, "mirror")

    # get the mirror
    mirror = dist.get("mirror", "")
    if modifier:
        mirror = modifier["mirror"]

    if mirror:
        print(mirror)

def get_security_mirror(args):
    dist = YAML_DATA.get(args.dist, {})
    aliases = dist.get("aliases", [])

    # if it's an alias, resolve it
    if args.release in aliases:
        release = resolve_alias(dist["mirror"], args.release)
        if not release:
            return
        args.release = release

    # get the modifier that applies, if any
    modifier = get_until_modifier(dist, args.release, "security_mirror")

    # get the mirror
    mirror = dist.get("security_mirror", "")
    if modifier:
        mirror = modifier["security_mirror"]

    if mirror:
        print(mirror)

def get_components(args):
    dist = YAML_DATA.get(args.dist, {})
    aliases = dist.get("aliases", [])

    # if it's an alias, resolve it
    if args.release in aliases:
        release = resolve_alias(dist["mirror"], args.release)
        if not release:
            return
        args.release = release

    # get the modifier that applies, if any
    modifier = get_until_modifier(dist, args.release, "components")

    # get the components
    components = dist.get("components", [])
    if modifier:
        components = modifier["components"]

    if components:
        print(" ".join(components))

def get_template(args):
    dist = YAML_DATA.get(args.dist, {})
    aliases = dist.get("aliases", [])

    # if it's an alias, resolve it
    if args.release in aliases:
        release = resolve_alias(dist["mirror"], args.release)
        if not release:
            return
        args.release = release

    # get the modifier that applies, if any
    modifier = get_until_modifier(dist, args.release, "template")

    # get the template
    template = dist.get("template", args.dist)
    if modifier:
        template = modifier["template"]

    if template:
        print(template)

def get_keyrings(args):
    dist = YAML_DATA.get(args.dist, {})
    aliases = dist.get("aliases", [])

    # if it's an alias, resolve it
    if args.release in aliases:
        release = resolve_alias(dist["mirror"], args.release)
        if not release:
            return
        args.release = release

    # get the modifier that applies, if any
    modifier = get_until_modifier(dist, args.release, "keyrings")

    # get the keyrings and mirror
    default_keyrings = [{"package": f"{args.dist}-archive-keyring"}]
    keyrings = dist.get("keyrings", default_keyrings)
    if modifier:
        keyrings = modifier["keyrings"]

    for k in keyrings:
        if "url" in k:
            print(k["url"])
        else:
            package = k["package"]
            mirror = k.get("mirror", dist["mirror"])
            files = k.get("files", [f"{package}.gpg"])
            print(f"{package} {mirror} {' '.join(files)}")

def dump_yaml(args):
    class MyDumper(yaml.SafeDumper):
        # substitute anchors and aliases, cf:
        # https://github.com/yaml/pyyaml/issues/535#issuecomment-1293636712
        def ignore_aliases(self, data):
            return True
        # indent arrays, cf:
        # https://github.com/yaml/pyyaml/issues/234#issuecomment-765894586
        def increase_indent(self, flow=False, *args, **kwargs):
            return super().increase_indent(flow=flow, indentless=False)

    yaml.dump(YAML_DATA, sys.stdout, sort_keys=False, Dumper=MyDumper)


# main


with open(YAML_FILE) as f:
    environment = jinja2.Environment(loader=jinja2.FileSystemLoader("."))
    template = environment.get_template("mkchdist.yaml")
    content = template.render()
    YAML_DATA = yaml.safe_load(content)

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(required=True)

commands = [
    ("dump", [], dump_yaml),
    ("dists", [], get_dists),
    ("releases", ["dist"], get_releases),
    ("aliases", ["dist"], get_aliases),
    ("mirror", ["dist", "release"], get_mirror),
    ("security_mirror", ["dist", "release"], get_security_mirror),
    ("components", ["dist", "release"], get_components),
    ("template", ["dist", "release"], get_template),
    ("keyrings", ["dist", "release"], get_keyrings),
]

for c in commands:
    p = subparsers.add_parser(c[0])
    for arg in c[1]:
        p.add_argument(arg)
    p.set_defaults(func=c[2])

args = parser.parse_args()
args.func(args)
