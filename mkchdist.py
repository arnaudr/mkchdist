#!/usr/bin/python3

import argparse

EPILOG = """DEFAULT TARGET
  If TARGET is not specified via the -t option, then a default target name is
  created according to the following rules:
    * if the distro is debian: target is set to RELEASE
    * if the release name already starts with DISTRO-: target is set RELEASE
    * otherwise target is set to DISTRO-RELEASE
    * the suffif -ARCH is appended in case of foreign architecture."""

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter,
    epilog=EPILOG)

parser.add_argument("-a", "--architecture",
    help="Choose architecture (default: dpkg --print-architecture)")
parser.add_argument("-d", "--data-dir",
    help="Directory to create the chdist into (default: ~/.chdist/)")
parser.add_argument("-f", "--force", action="store_true",
    help="Overwrite destination if it already exists")
parser.add_argument("-t", "--target",
    help="Name of the chdist, ie. directory created under DATA_DIR")
parser.add_argument("--no-apt-update", action="store_true",
    help="Don't run 'apt update' and 'apt-file update' at the end")
parser.add_argument("--no-deb-src", action="store_true",
    help="Don't add deb-src to the sources.list")
parser.add_argument("distro")
parser.add_argument("release")
parser.add_argument("components")

args = parser.parse_args()
