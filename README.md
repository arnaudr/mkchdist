# mkchdist

Get ready:

```
sudo apt install \
    python3 python3-jinja2 python3-yaml \
    ca-certificates dctrl-tools file j2cli wget
```

Then run `./mkchdist` and follow the instructions.

By default mkchdist's output dir is `~/.chdist/`, so for a test you might
prefer to run `./mkchdist --data-dir out`.
